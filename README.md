# Setup

Clone Repo
Do `npm install`
Do `npm start` (For development env) or Do `npm run build` (For production, Build be available in `build` folder)

# Config

You can set api base url at `src/index.js` by updating following line:

axios.defaults.baseURL  =  '{baseurl}';