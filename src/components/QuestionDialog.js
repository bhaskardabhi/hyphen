import React, { Component } from "react";
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';

class QuestionDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      question: {
        title: '',
        answers: [{title: ''}]
      }
    };

    this.addAnswer = this.addAnswer.bind(this);
  }

  addAnswer(){
    this.state.question.answers.push({title: ''});

    this.setState({
      question: {
        ...this.state.question,
        answers: this.state.question.answers
      }
    });
  }

  saveQuestion(){
    var that = this;
    var question = this.state.question;

    axios.post({
      method: question.id ? 'put':'post',
      url: question.id ? 'api/questions/' + question.id : 'api/questions',
      data: question
    })
    .then(function (response) {
      this.props.onModalClose();
    })
    .catch(function (error) {
      alert("Error saving question");
    });
  }

  render() {
    return (
      <Modal show={this.props.mode} onClick={this.props.onModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.mode == 'add' && <h3>Add a custom question</h3>}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div class="form-group">
              <label for="question">Question</label>
              <input class="form-control" id="question" aria-describedby="Enter your question" placeholder="Enter your question" />
            </div>
            <div class="form-group">
              <label>Answers</label>
              {this.state.question.answers.map((answer,index) => 
                <input class="form-control" placeholder={"Answer " + (index + 1)} />
              )}
            </div>
            <button type="button" class="btn btn-primary" onClick={this.addAnswer}>Add new answer</button>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <button type="button" class="btn btn-primary" onClick={this.saveQuestion}>Save Changes</button>
        </Modal.Footer>
      </Modal>
    );
  }
}
export default QuestionDialog;