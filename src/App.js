import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import QuestionDialog from "./components/QuestionDialog";
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      questions: [],
      mode: null
    };

    this.addQuestion = this.addQuestion.bind(this);
  }

  componentDidMount(){
    axios.get("api/questions?with=answers").then(response => {
      this.setState({
        questions: response.data.data,
        current_page: response.data.current_page,
        total_page: Math.ceil(response.data.total / response.data.per_page),
        per_page: response.data.per_page
      });
    }).catch(() => alert("There was error fetching questions"));
  }

  addQuestion(){
    this.setState({mode: 'add'});
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="w-100"> 
            <h2>Custom Questions</h2>
            <p>You must add at least one question to lauch a survey</p>
            <div className="container">
              <div className="row">
                <div className="col"><strong>Question</strong></div>
                <div className="col"><strong>Question Settings</strong></div>
              </div>
            </div>
          </div>
          {this.state.questions.map((question, index) => 
            <div key={index} className="card w-100">
              <div className="card-body">
                <h5 className="card-title">{question.title}</h5>
                <p className="card-text">
                  {question.answers.map((answer, index) =>
                    <span key={index}>
                      <span>{answer.title}</span><span className="spacer">{(index != question.answers.length - 1) ? "*" : ''}</span>
                    </span>
                  )}
                </p>
              </div>
            </div>
          )}
          {this.state.questions.length == 0 && 
            <div className="card w-100">
              <div className="card-body">
                <p className="card-text">
                  No Data
                </p>
              </div>
            </div>
          }
          <div className="w-100">
            <button type="button" className="btn btn-primary" onClick={this.addQuestion}>Add Question</button>
          </div>
        </div>
        {this.state.mode && 
          <QuestionDialog 
            mode={this.state.mode} 
            onModalClose={this.onModalClose}/>
        }
      </div>
    );
  }
}
export default App;
